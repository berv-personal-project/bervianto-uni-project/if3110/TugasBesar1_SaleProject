-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 08 Okt 2016 pada 12.57
-- Versi Server: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `saleproject`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `account`
--

CREATE TABLE `account` (
  `id` int(11) NOT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `postalcode` varchar(10) DEFAULT NULL,
  `phonenumber` varchar(13) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `account`
--

INSERT INTO `account` (`id`, `fullname`, `username`, `email`, `password`, `address`, `postalcode`, `phonenumber`) VALUES
(1, 'Bervianto Leo Pratama', 'leopratama', 'bervianto.leo@saleproject.com', 'justforfun', 'Jalan Andir No.10. Bandung', '40132', '08189283612'),
(2, 'Satya', 'satya', 'satya@saleproject.com', 'satya', 'Jalan Jumbawa No.100. Jakarta Utara', '30127', '081263638292'),
(3, 'Budi', 'budi', 'budi@saleproject.com', 'budi', 'Jalan Andawa No.90. Jakarta Pusat', '40122', '081628303272'),
(4, 'Jook', 'jook', 'jook@jook.com', 'jook', 'jook', '40188', '82722133131'),
(5, 'test', 'test', 'test@test.com', 'Test', 'Test', '40127', '201272772'),
(6, 'Santomo', 'santomo', 'santomo@santomo.com', 'santomo', 'Santomo Ternama', '10293', '12873783991'),
(7, 'Banana', 'banana', 'banana@banana.com', 'Banana', 'Jalan Banana Indah Bikin Lapar. Banana Town', '11111', '088888888888');

-- --------------------------------------------------------

--
-- Struktur dari tabel `buys`
--

CREATE TABLE `buys` (
  `id` int(11) NOT NULL,
  `id_item` int(11) DEFAULT NULL,
  `id_buyer` int(11) DEFAULT NULL,
  `id_seller` int(11) DEFAULT NULL,
  `photo` varchar(200) NOT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `item_price` int(11) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `consignee` varchar(255) DEFAULT NULL,
  `full_address` varchar(500) DEFAULT NULL,
  `postal_code` varchar(10) DEFAULT NULL,
  `phone_number` varchar(13) DEFAULT NULL,
  `cc_number` varchar(30) DEFAULT NULL,
  `verification_code` varchar(3) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `time` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `buys`
--

INSERT INTO `buys` (`id`, `id_item`, `id_buyer`, `id_seller`, `photo`, `item_name`, `item_price`, `quantity`, `consignee`, `full_address`, `postal_code`, `phone_number`, `cc_number`, `verification_code`, `date`, `time`) VALUES
(1, 1, 7, 4, './images/Dodol-Garut.jpg', 'Dodol Garut', 65000, 4, 'Banana', 'Jalan Banana Indah Bikin Lapar. Banana Town..', '11113', '08888888888', '123456789012', '333', 'Saturday, 8 October 2016', '17.7'),
(2, 11, 7, 2, './images/Dodol_susu.JPG', 'Dodol', 50000, 1, 'Banana', 'Jalan Banana Indah Bikin Lapar. Banana Town', '11111', '088888888888', '123456789012', '123', 'Saturday, 8 October 2016', '17.26'),
(3, 12, 5, 4, './images/cumi-lezat.jpg', 'Cumi-Cumi', 128000, 1, 'test', 'Test', '40127', '201272772', '123456789012', '123', 'Saturday, 8 October 2016', '17.42');

-- --------------------------------------------------------

--
-- Struktur dari tabel `item`
--

CREATE TABLE `item` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `price` bigint(20) DEFAULT NULL,
  `photo` varchar(500) DEFAULT NULL,
  `seller` int(11) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `time` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `item`
--

INSERT INTO `item` (`id`, `name`, `description`, `price`, `photo`, `seller`, `date`, `time`) VALUES
(1, 'Dodol Garut', 'Dodol Garut Terenak Sedunia', 65000, './images/Dodol-Garut.jpg', 4, 'Saturday, 8 October 2016', '17.33'),
(12, 'Cumi-Cumi', 'Cumi cumi terbaik bangsa', 128000, './images/cumi-lezat.jpg', 4, 'Saturday, 8 October 2016', '17.36');

-- --------------------------------------------------------

--
-- Struktur dari tabel `likes`
--

CREATE TABLE `likes` (
  `id` int(11) NOT NULL,
  `id_item` int(11) DEFAULT NULL,
  `id_account` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `likes`
--

INSERT INTO `likes` (`id`, `id_item`, `id_account`) VALUES
(13, 1, 6),
(14, 11, 3),
(20, 1, 7);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `buys`
--
ALTER TABLE `buys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `buys`
--
ALTER TABLE `buys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
