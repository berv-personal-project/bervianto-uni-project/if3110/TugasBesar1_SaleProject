<!DOCTYPE html> 
<html> 
	<head> 
		<title>Edit Product</title> 
		<link rel="stylesheet" type="text/css" href="css/main.css">
		<script type="text/javascript" src="js/validate.js"></script>
	</head> 
	<body id="body-color"> 
		<?php
 			include ('include/connect.php');
 			include ('include/functions.php');
 			$connect = connectToDatabase();
 			$id = $_GET["id_active"];
 			$iditem = $_GET["id_item"];
 			$name = getName($connect);
 			$product = getProduct($connect,$iditem);	
 			function edit_product($id,$iditem,$time,$connect) {
 				//New Data
 				$queryupdate = "UPDATE item SET name='".$_POST['name']."', description='".$_POST['description']."', price='".$_POST['price']."' WHERE id=".$iditem."";
 				$res = mysqli_query($connect,$queryupdate) or die(mysqli_error($connect));  
 
 				mysqli_close($connect);
 				echo "<meta http-equiv='refresh' content='0; URL=./your_products.php?id_active=$id'>";
 			}
 			/*if(isset($_POST["add_product"]) && isset($_FILES['photo'])) {
     $file_temp = $_FILES['file']['tmp_name'];   
     $info = getimagesize($file_temp);
 } else {
     print "File not sent to server succesfully!";
 }*/
 			if(isset($_POST['edit_product'])) {
 				if (!empty($_POST['name']) && !empty($_POST['description']) && !empty($_POST['price']))
 				{
 					if (!$connect) {
 						die('Connection Failed'.mysqli_error());
 					} else {
 						edit_product($id,$iditem,$time,$connect);
 					}
 				}
 			}	
 		?>
 		<div>
 			<h1> <font color="#8a1b14">Sale</font><font color="#6e95e4">Project</font> </h1>
 		</div>
		<div class="atas">
			Hi, <?php echo $name["username"];?>!<br>
			<a href="sign-in.php" class="logout"><font size="1">logout</font></a>
		</div>
		<div class="navbar">
		<ul>
			<?php
 			echo '<li><a href="catalog.php?id_active='.$id.'">Catalog</a></li>
 				<li><a href="your_products.php?id_active='.$id.'">Your Products</a></li>
 				<li><a href="add_product.php?id_active='.$id.'">Add Products</a></li>
 				<li><a href="sales.php?id_active='.$id.'">Sales</a></li>
 				<li><a href="purchases.php?id_active='.$id.'">Purchases</a></li>';
 			?>
		</ul>
		</div>
		<br>
		<div>
			<h2> Please update your product here </h2>
		</div>

		<?php echo '<form method="POST" id="edit_product" action="edit_product.php?id_active='.$id.'&id_item='.$iditem.'" onsubmit="return validateEditProduct()">'; ?>
			<label for="product_name">Name</label><br>
			<?php echo '<input type="text" name="name" id="name" value="'.$product["name"].'"><br>
			<div id="notif-name" class="notif">
			</div>
			<label for="product_desc">Description (max 200 chars)</label><br>
			<textarea rows="5" cols="107" name="description" id="description">'.$product["description"].'</textarea><br>
			<div id="notif-description" class="notif">
			</div>
			<label for="product_price">Price (IDR)</label><br>
			<input type="text" name="price" id="price" value="'.$product["price"].'"><br>
			<div id="notif-price" class="notif">
			</div>
			<label for="product_photo">Photo</label><br>
			<input type="file" accept="image/*" align="middle" name="photo" id="photo" disabled><br>'; ?>
			<div class="notif" id="notif">
				 <p>* Sorry, you can't edit your product photo.</p>
			</div>
			<div class="submit-style">
				<input id="button" type="submit" name="edit_product" value="UPDATE"> 
				<input id="cancel" class="cancel" type="button" size="20" value="CANCEL" onclick="window.location.href='your_products.php?id_active=<?php echo $id;?>'"> 
			</div>
		</form>	
	</body> 
</html>

