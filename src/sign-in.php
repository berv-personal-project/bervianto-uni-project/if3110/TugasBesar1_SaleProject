<!DOCTYPE HTML> 
<html> 
	
	<head> 
		<title>Sign-In</title>
    <meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.min.css">
		<script type="text/javascript" src="js/validate.js"></script>
	</head> 
	<body id="body-color"> 

		<?php 
			include ('include/connect.php');
			include ('include/functions.php');
			/* $ID = $_POST['user']; $Password = $_POST['pass']; */ 
			function SignIn() { 
				$connect = connectToDatabase();
				if (!$connect) {
					//SignIn();
				}
				if(!empty($_GET['username'])) //checking the 'user' name which is from Sign-In.html, is it empty or have some text 
				{ 
					$query = mysqli_query($connect,"SELECT * FROM account where username = '$_POST[username]' AND password = '$_POST[password]'") or die(mysqli_error($connect)); 
					$row = mysqli_fetch_array($query,MYSQLI_ASSOC); 
					$id = $row['id']; 
					if(!empty($row['username']) AND !empty($row['password'])) { 
						mysqli_close($connect);
						echo "<meta http-equiv='refresh' content='0; URL=./catalog.php?id_active=$id'>"; /* Redirect browser */
					}
					else { 
						echo "<meta http-equiv='refresh' content='0; URL=./sign-in.php'>"; 
					} 
				}
			} 
			if(isset($_POST['login'])) { SignIn(); } 
		?>
	<section class="section">
	<div class="container">
		<div class="box">
			<h1 class="title has-text-centered"> <font color="#8a1b14">Sale</font><font color="#6e95e4">Project</font> </h1>
			<h2 class="subtitle has-text-centered"> Please login </h2>
		
		<form  method="POST" action="sign-in.php" onsubmit="return validateSignIn()"> 
			<div class="field">
				<label class="label" for='username'> Username/Email </label>	
			  <p class="control has-icons-left has-icons-right">
					<span class="icon is-small is-left">
						<i class="fas fa-envelope"></i>
					</span>
					<input class="input" type="text" id="username" name="username" placeholder="Username/Email">
					<div id="notif-username" class="notif"></div>
				</p>
			</div>
			<div class="field">
				<label class="label" for='password'> Password </label>	
				<p class="control has-icons-left has-icons-right">
					<span class="icon is-small is-left">
						<i class="fas fa-lock"></i>
					</span>
					<input class="input" type="password" id="password" name="password"  placeholder="Password"><br>
					<div id="notif-password" class="notif"></div>
				</p>
			</div>
			<div class="submit-style">
				<input class="button is-success" id="button" type="submit" name="login" value="LOGIN"> 
			</div>
		</form>
		</div>
			</section>
			<section class="section">
			<div class="subtitle has-text-centered"> 
					<b>Dont have an account yet? Register <a class="link-register" href="register.php">here</a><b>
			</div>	
			</section>
		</div>
	</body> 
</html>