function getLike(id,item) {				
  	if (window.XMLHttpRequest) {
    	// code for IE7+, Firefox, Chrome, Opera, Safari
    	xmlhttp=new XMLHttpRequest();
  	} else {  // code for IE6, IE5
    	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  	}
  	xmlhttp.onreadystatechange=function() {
    	if (this.readyState==4 && this.status==200) {
      		document.getElementById("detail"+item).innerHTML=this.responseText;
    	}
  	}
  	xmlhttp.open("GET","like.php?id="+id+"&item="+item,true);
  	xmlhttp.send();
}

function getDislike(id,item) {
			
	if (window.XMLHttpRequest) {
		// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
  	} else {  // code for IE6, IE5
    	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  	}
  	xmlhttp.onreadystatechange=function() {
    	if (this.readyState==4 && this.status==200) {
    		document.getElementById("detail"+item).innerHTML=this.responseText;
    	}
  	}
  	xmlhttp.open("GET","dislike.php?id="+id+"&item="+item,true);
  	xmlhttp.send();
}

function countTotalPrice(item_price) {
	var quantity = document.getElementById("quantity").value;
	if (isNaN(quantity*item_price)) {
		document.getElementById("total-price").innerHTML = 0;
	} else {
		document.getElementById("total-price").innerHTML = showPrice(quantity*item_price);
	}
}

function showPrice(price) {
	if (price<1000) {
		return price;
	} else {
		var temp="";
		do {
			var satuan = price % 1000;
			if (satuan<10) {
				temp = ".00"+satuan.toString()+temp;
			} else if ($satuan<100) {
				temp = ".0"+satuan.toString()+temp;
			} else {
				temp = "."+satuan.toString+$temp;
			}
			price = price/1000;
		} while (price>=1000);
		temp = price.toString()+temp;
		return temp;
	}
}