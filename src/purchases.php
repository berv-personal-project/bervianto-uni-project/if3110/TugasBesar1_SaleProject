<?php
			include ('include/connect.php');
			include ('include/functions.php');
			
			$connect = connectToDatabase();
			
			$id = $_GET['id_active'];
			$name = getName($connect);
		?>


<!DOCTYPE HTML> 
<html> 
	<head> 
		<title>Purchases</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.min.css">
		
	</head> 

	<header>
		<nav class="navbar" role="navigation" aria-label="main navigation">
			<div class="navbar-brand">
				<a class="navbar-item" href="/">
					
					<h1 class="title"> <font color="#8a1b14">Sale</font><font color="#6e95e4">Project</font> </h1>
				</a>

				<a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
					<span aria-hidden="true"></span>
					<span aria-hidden="true"></span>
					<span aria-hidden="true"></span>
				</a>
			</div>

			<div id="navbarBasicExample" class="navbar-menu">
				<div class="navbar-start">
					<?php
					echo '<a class="navbar-item" href="catalog.php?id_active='.$id.'">Catalog</a>
						<a class="navbar-item" href="your_products.php?id_active='.$id.'">Your Products</a>
						<a class="navbar-item" href="add_product.php?id_active='.$id.'">Add Products</a>
						<a class="navbar-item" href="sales.php?id_active='.$id.'">Sales</a>
						<a class="navbar-item is-active" href="purchases.php?id_active='.$id.'">Purchases</a>';
					?>
					
				</div>

				<div class="navbar-end">
					<div class="navbar-item">
						Hi, <?php echo $name["username"];?>!
					</div>
					<div class="navbar-item">
						<a href="sign-in.php" class="button is-danger">logout</a>
					</div>
				</div>
			</div>
		</nav>
	</header>

	<body id="body-color"> 
		<section class="section">
			<div class="container">
			<h2 class="title has-text-centered"> Here are your purchases </h2>
				<?php 
					$sql = "SELECT * FROM buys WHERE id_buyer='$id'"; 
					$r_query = mysqli_query($connect,$sql) or die(mysql_error());
					while ($row = mysqli_fetch_assoc($r_query)){  
				?>
				<div class="filter">
					<!-- HARUSNYA ADA TANGGAL DI BUYS -->
					<div class="product-view">
						<?php echo $row["date"] ?> <br> at <?php echo $row["time"] ?>
					</div>
					<div class="product-view">
						<div class="photo">
							<img src=<?php echo $row["photo"]; ?> alt="Mountain View" width="100px" height="100px">
						</div>
						<div class="description-sales">
							<?php
								echo "<b>".$row["item_name"]."</b><br>";
								echo "IDR ".showPrice($row["quantity"]*$row["item_price"])."<br>";
								echo $row["quantity"]." pcs<br>";
								echo "@IDR ".showPrice($row["item_price"])."<br>";
								echo "<br>";
								$queryseller = "SELECT username FROM account WHERE id ='".$row["id_seller"]."'";
								$resultseller = mysqli_query($connect,$queryseller) or die(mysqli_error());	
								$name = mysqli_fetch_assoc($resultseller);	
								echo "<font size='1'>bought from <b>".$name["username"]."</b></font>";
							?>
						</div>
						<div class="detail-sales">
							Delivery to <b><?php echo $row["consignee"] ?></b><br>
							<?php echo $row["full_address"] ?><br>
							<?php echo $row["postal_code"] ?><br>
							<?php echo $row["phone_number"] ?><br>
						</div>
					</div>
				</div>
				<?php
					}
				?>

			</div>
		</section>
	</body>
</html>
