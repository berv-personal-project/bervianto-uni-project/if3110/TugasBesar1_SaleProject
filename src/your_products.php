<?php
			include ('include/connect.php');
			include ('include/functions.php');
			$connect= connectToDatabase();
			$id = $_GET['id_active'];
			if(isset($_GET['delete_item'])) {
				$delete_item = $_GET['delete_item'];
				$delete = mysqli_query($connect,"DELETE FROM item WHERE id=$delete_item");
				echo "<meta http-equiv='refresh' content='0; URL=./your_products.php?id_active=$id'>";
			}
			$sql = "SELECT * FROM item WHERE seller='$id'";
			$result = mysqli_query($connect,$sql);
			$name = getName($connect);
			
		?>

<!DOCTYPE HTML> 
<html> 
	<head> 
		<title>Your Products</title> 
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.min.css">  
	</head> 


	<header>
		<nav class="navbar" role="navigation" aria-label="main navigation">
			<div class="navbar-brand">
				<a class="navbar-item" href="/">
					
					<h1 class="title"> <font color="#8a1b14">Sale</font><font color="#6e95e4">Project</font> </h1>
				</a>

				<a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
					<span aria-hidden="true"></span>
					<span aria-hidden="true"></span>
					<span aria-hidden="true"></span>
				</a>
			</div>

			<div id="navbarBasicExample" class="navbar-menu">
				<div class="navbar-start">
					<?php
					echo '<a class="navbar-item" href="catalog.php?id_active='.$id.'">Catalog</a>
						<a class="navbar-item is-active" href="your_products.php?id_active='.$id.'">Your Products</a>
						<a class="navbar-item" href="add_product.php?id_active='.$id.'">Add Products</a>
						<a class="navbar-item" href="sales.php?id_active='.$id.'">Sales</a>
						<a class="navbar-item" href="purchases.php?id_active='.$id.'">Purchases</a>';
					?>
					
				</div>

				<div class="navbar-end">
					<div class="navbar-item">
						Hi, <?php echo $name["username"];?>!
					</div>
					<div class="navbar-item">
						<a href="sign-in.php" class="button is-danger">logout</a>
					</div>
				</div>
			</div>
		</nav>
	</header>

	<body id="body-color"> 
			<section class="section">
			<h2 class="title has-text-centered"> What are you going to sell today? </h2>
		<div class="filter">
			<?php 
			if (mysqli_num_rows($result) > 0) {
			    // output data of each row
			    while($row = mysqli_fetch_assoc($result)) {
			?>
			<div class="product-view">
				<?php 
					$seller = $row['seller'];
					$sqlseller = "SELECT username FROM account WHERE id='$seller'";
					$resultseller = mysqli_query($connect,$sqlseller);
					while($rowseller = mysqli_fetch_assoc($resultseller)) {
						echo $rowseller["username"]; 
				echo 
				'<br>';
				echo $row["date"]; echo '<br> at '; echo $row["time"]; echo 
			'</div>
			<div class="product-view">
				<div class="photo">
					<img src='; echo $row["photo"]; echo ' alt="Mountain View" style="width:100px;height:100px;">
				</div>
				<div class="description">
					<b> '; echo $row["name"] ;echo'</b> <br>
					'; echo "IDR ".showPrice($row["price"]) ;echo'<br>
					<font size="1">  ';echo $row["description"] ;echo'</font>
				</div>
				<div class="detail">
					<div style="">
						<font size="1">'; echo ''.likes($connect,$row['id']).' likes </font>
					</div>
					<div style="margin-top:-5px;margin-bottom:20px;">
						<font size="1">';  echo ''.purchases($connect,$row['id']).' purchases </font><br>
					</div>
					<div style="display:inline-block;width:40%;">
						<b><a href="edit_product.php?id_active='.$id.'&id_item='.$row['id'].'" style="text-decoration:none;"><font color="#FF8C00" size="2">EDIT</font></a></b>	
					</div>

					<div style="display:inline-block;width:40%;">
						<b><a href="javascript:void(0)" onclick="deleteConfirmation('.$id.','.$row['id'].')" style="text-decoration:none;"><font color="#8B0000" size="2">DELETE</font></a></b>
					</div>
				</div>
			</div>';
					}
					}
					}
				?>
		</div>
		</form>	

			</section>
		<script>
			function deleteConfirmation(account_id,item_id) {
    			if (confirm("Are you sure deleting this item?") == true) {
        			window.location.href = "your_products.php?id_active="+account_id+"&delete_item="+item_id;
    			} else {
    
    			}
			}
		</script>
	</body>
</html>