<html> 
	<head> 
		<title>Confirm Purchase</title> 
		<link rel="stylesheet" type="text/css" href="css/main.css"> 
		<script type="text/javascript" src="js/validate.js"></script>
		<script type="text/javascript" src="js/function.js"></script>
	</head> 
	<?php 
			include ('include/connect.php');
			include ('include/functions.php');
			$connect = connectToDatabase();
			$id = $_GET["id_active"];
			$name = getName($connect);
			$id_item = $_GET["id_item"];
			
			$item = getProduct($connect,$id_item);
			$user = getUserData($connect);
	?>
	
	<body id="body-color" onload="countTotalPrice('<?php echo $item['price'];?>')"> 
		<?php
			
			$queryseller = "SELECT * FROM item WHERE id=".$id_item."";
			$ressell = mysqli_query($connect,$queryseller) or die(mysqli_error($connect));
			while ($rowsell = mysqli_fetch_assoc($ressell)){  
				$id_seller = $rowsell['seller'];
				$name_item = $rowsell['name'];
			}
			
			function purchase($id,$id_item,$id_seller,$photo,$name_item,$item_price,$date,$time,$connect) {
				$query = "INSERT into BUYS (id_item, id_buyer, id_seller, photo, item_name, item_price, quantity, consignee, full_address, postal_code, phone_number, cc_number, verification_code, date, time) values(".$id_item.",".$id.",".$id_seller.",'".$photo."','".$name_item."','".$item_price."',".$_POST['quantity'].", '".$_POST['fullname']."','".$_POST['address']."','".$_POST['postalcode']."','".$_POST['phonenumber']."','".$_POST['creditcardnumber']."','".$_POST['creditcardvalue']."','".$date."','".$time."')"; 
				$res = mysqli_query($connect,$query) or die(mysqli_error($connect)); 
				mysqli_close($connect);
				
				echo "<meta http-equiv='refresh' content='0; URL=./purchases.php?id_active=$id'>";
			}
			
			if(isset($_POST['confirm'])) {
				
				if (!$connect) {
					die('Connection Failed'.mysqli_error());
				} else {
					date_default_timezone_set('Asia/Jakarta');
					$today = getdate();
					$day = $today['weekday'];
					$d = $today['mday'];
					$m = $today['month'];
					$y = $today['year'];
					$h = $today['hours'];
					$min = $today['minutes'];
						
					$date = $day.", ".$d." ".$m." ".$y;
					if ($min<10) {
						if ($h<10) {
							$time = "0".$h.".0".$min;
						}
						else {
							$time = $h.".0".$min;
						}
					}
					else {
						if ($h<10) {
							$time = "0".$h.".".$min;
						}
						else {
							$time = $h.".".$min;
						}
					}
					purchase($id,$id_item,$id_seller,$item["photo"],$name_item,$item["price"],$date,$time,$connect);
				}					
			}
		?>
		<div>
			<h1> <font color="#8a1b14">Sale</font><font color="#6e95e4">Project</font> </h1>
		</div>
		<div class="atas">
			Hi, <?php echo $name["username"];?>!<br>
			<a href="sign-in.php" class="logout"><font size="1">logout</font></a>
		</div>
		<div class="navbar">
		<ul>
			<?php
			echo '<li><a href="catalog.php?id_active='.$id.'">Catalog</a></li>
				<li><a href="your_products.php?id_active='.$id.'">Your Products</a></li>
				<li><a href="add_product.php?id_active='.$id.'">Add Products</a></li>
				<li><a href="sales.php?id_active='.$id.'">Sales</a></li>
				<li><a href="purchases.php?id_active='.$id.'">Purchases</a></li>';
			?>
		</ul>
		<br><br>
		</div>
		<div>
			<h2> Please confirm your purchase </h2>
		</div>
		
		<?php echo '<form method="POST" id="confirm" action="confirm_purchase.php?id_active='.$id.'&id_item='.$id_item.'" onsubmit="return validatePurchase();">'; ?>
			<label>Product : <?php echo $item["name"]?></label> <br>
			<label>Price : IDR <div id="item-price" class="show"><?php echo showPrice($item["price"]);?></div></label>  <br>
			<label for="quantity">Quantity : 
			<input id="quantity" class="quantity" type="text" name="quantity" value="1" onchange="countTotalPrice('<?php echo $item['price'];?>')"> pcs</label> <br>
			<label>Total Price : IDR <div id="total-price" class="show"></div></label> <br>
			<label> Delivery to : </label> <br>
			<br>
			
			<label for="consignee"> Consignee </label> <br>
			<input id="fullname" type="text" name="fullname" value="<?php echo $user["fullname"]?>" onchange="return validateFullName(this.value)"> <br>
			<div id="notif-fullname" class="notif">
			</div>
			
			<label for='address'> Full Address </label><br>
			<textarea id="address" name="address"><?php echo $user["address"]?></textarea><br>
			<div id="notif-address" class="notif">
			</div>
			
			<label for="postalcode"> Postal Code </label> <br>
			<input id="postalcode" type="text" name="postalcode" value="<?php echo $user["postalcode"]?>" onchange="return validatePostalCode(this.value)"> <br>
			<div id="notif-postalcode" class="notif">
			</div>	
			
			<label for="phone"> Phone </label> <br>
			<input id="phonenumber" type="text" name="phonenumber" value="<?php echo $user["phonenumber"]?>" onchange="return validatePhoneNumber(this.value)"> <br>
			<div id="notif-phonenumber" class="notif">
			</div>
			
			<label for="creditcardnumber"> 12 Digits Credit Card Number </label> <br>
			<input id="creditcardnumber" type="text" name="creditcardnumber" onchange="return validateCCNumber()"> <br>
			<div id="notif-creditcardnumber" class="notif">
			</div>
			
			<label for="cardvalue"> 3 Digits Card Value </label> <br>
			<input id="creditcardvalue" type="text" name="creditcardvalue" onchange="return validateCCValue()"> <br>
			<div id="notif-creditcardvalue" class="notif">
			</div>
			
			<div class="submit-style">
				<input id="submit" type="submit" size="20" name="confirm" value="CONFIRM"> 
				<input id="cancel" class="cancel" type="button" size="20" value="CANCEL" onclick="window.location.href='catalog.php?id_active=<?php echo $id;?>'"> 
			</div>
		</form>	
	</body> 
</html>

