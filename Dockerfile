FROM php:7-fpm-alpine

RUN apk update && apk add build-base

RUN apk add mysql && docker-php-ext-install pdo pdo_mysql mysqli