# Tampilan Aplikasi

### Halaman Sign In

#### 1. Belum Terisi
![Belum terisi](/Screenshot/sign-in-empty.png "Belum terisi")

#### 2. Data Invalid
![Data invalid](/Screenshot/sign-in-invalid.png "Data invalid")

### Halaman Register

#### 1. Belum Terisi
![Belum terisi](/Screenshot/sign-up-empty-scaled-67.png "Belum terisi")

#### 2. Data Invalid
![Data invalid](/Screenshot/sign-up-invalid.png "Data invalid")


### Halaman Catalog

#### 1. Catalog Home
![Catalog Home](/Screenshot/catalog-liked.png "Catalog Home")

#### 2. Search By Product
![Search By Product](/Screenshot/catalog-searchby-product.png "Search By Product")

#### 3. Search By Store
![Search By Store](/Screenshot/catalog-searchby-store.png "Search By Store")

### Halaman Add Product

#### 1. Data Valid - Pengisian
![Valid](/Screenshot/add-product-valid.png "Valid")

#### 2. Data Invalid
![Data invalid](/Screenshot/add-product-invalid.png "Data invalid")

### Halaman My Product

#### 1. Halaman ada Produk yang dijual
![Ada produk](/Screenshot/your-product-haved.png "Ada produk")

#### 2. Halaman tidak menjual produk dan setelah menghapus satu produk (jika hanya satu)
![Tidak ada produk](/Screenshot/your-product-deleted-and-empty.png "Tidak ada produk")

### Halaman produk yang sudah dijual

#### 1. Ada yang dijual
![Ada produk](/Screenshot/sales-haved.png "Ada produk")

#### 2. Tidak ada yang dijual
![Tida ada produk](/Screenshot/sales-empty.png "Tidak ada produk")


### Halaman produk yang sudah dibeli

#### 1. Ada yang dibeli
![Ada produk](/Screenshot/purchases-haved.png "Ada produk")

#### 2. Tidak ada yang dibeli
![Tidak ada produk](/Screenshot/purchases-empty.png "Tidak ada produk")

### Halaman Edit Product

![Edit produk](/Screenshot/update-product.png "Edit produk")


### Halaman Konfirmasi Pembelian

#### 1. Data Tidak Valid
![Data Tidak Valid](/Screenshot/purchase-invalid.png "Purchase Tidak Valid")

#### 2. Data Valid dan Konfirmasi
![Data Valid](/Screenshot/purchase-confirm.PNG "Data valid dan konfirmasi")

___

# Pembagian Tugas

### Tampilan

1. Login: 13514087
2. Register: 13514047
3. Halaman Purchases: 13514047
4. Edit Product: 13514047
5. Catalog : 13514087
6. Sales : 13514087
7. Confirm Purchase : 13514047
8. Add Product : 13514081
9. Edit Product : 13514047
10. Your Products : 13514081
11. Purchases : 13514047

### Fungsionalitas

1. Login: 13514087
2. Register: 13514047
3. Halaman Purchases: 13514047
4. Edit Product: 13514047, 13514081
5. Catalog : 13514087, 13514081
6. Sales : 13514087
7. Confirm Purchase : 135414047,13514087
8. Add Product : 13514081
9. Edit Product : 13514047, 13514081
10. Your Products : 13514081
11. Purchases : 13514047

# About

iDuar
1. Bervianto Leo P - 13514047
2. Ahmad Faiq Rahman - 13514081
3. Praditya Raudi A - 13514087